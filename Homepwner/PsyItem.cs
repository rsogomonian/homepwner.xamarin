﻿using System;
using MonoTouch.Foundation;

namespace Homepwner
{
	public class PsyItem
	{
		public String itemName;
		public String serialNumber;
		public int valueInDollars;
		public DateTime dateCreated;
		private static Random random = new Random ();

		public PsyItem () : this("Item", 0, "") { }

		public PsyItem (String name) : this(name, 0, "") { }

		public PsyItem (String name, int value) : this(name, value, "") { }

		public PsyItem (String name, int value, String serial)
		{
			itemName = name;
			serialNumber = serial;
			valueInDollars = value;

			dateCreated = new DateTime ();
		}

		public override String ToString ( )
		{
			return String.Format ("{0} ({1}): Value ${2}", itemName, serialNumber, valueInDollars);
		}

		public PsyItem RandomItem( )
		{
			String[] randomAdjectiveList = { "Fluffy", "Rusty", "Shiny", "Tinted", "Circular", "Flaming" };
			String[] randomNounList = { "Bear", "Spork", "Mac", "Beer", "Mirror", "Greaves" };

			int adjectiveIndex = random.Next(0, randomAdjectiveList.Length);
			int nounIndex = random.Next(0, randomNounList.Length);

			String randomName = randomAdjectiveList [adjectiveIndex] + " " + randomNounList [nounIndex];
			int randomValue = random.Next(0, 1001);
			String randomSerialNumber = String.Format("{0}{1}{2}{3}{4}",
				(char)('0' + random.Next(0, 10)),
				(char)('A' + random.Next(0, 26)),
				(char)('0' + random.Next(0, 10)),
				(char)('A' + random.Next(0, 26)),
				(char)('0' + random.Next(0, 10)));

			PsyItem item = new PsyItem (randomName, randomValue, randomSerialNumber);

			return item;
		}
	}
}

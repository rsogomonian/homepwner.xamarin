﻿using System;
using System.Drawing;
using MonoTouch.Foundation;
using MonoTouch.UIKit;

namespace Homepwner
{
	public class PsyItemViewControllerSource : UITableViewSource
	{
		public PsyItemViewControllerSource ()
		{
			PsyItemStore itemStore = new PsyItemStore ();
			for (int i = 0; i < 20; i++) {
				itemStore.createItem ();
			}
		}

		public override int NumberOfSections (UITableView tableView)
		{
			// TODO: return the actual number of sections
			return 1;

		}

		public override int RowsInSection (UITableView tableview, int section)
		{
			// TODO: return the actual number of items in the section
			PsyItemStore itemStore = new PsyItemStore ();
			PsyItem[] items = itemStore.allItems ();

			return items.Length;
		}

		/*
		public override string TitleForHeader (UITableView tableView, int section)
		{
			return "Header";
		}

		public override string TitleForFooter (UITableView tableView, int section)
		{
			return "Footer";
		}
		*/
		public override UITableViewCell GetCell (UITableView tableView, NSIndexPath indexPath)
		{
			var cell = tableView.DequeueReusableCell (PsyItemViewControllerCell.Key) as PsyItemViewControllerCell;
			if (cell == null)
				cell = new PsyItemViewControllerCell ();
			
			// TODO: populate the cell with the appropriate data based on the indexPath
			
			return cell;
		}
	}
}


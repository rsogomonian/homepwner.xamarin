﻿using System;
using System.Collections.Generic;

namespace Homepwner
{
	public class PsyItemStore
	{
		public static List<PsyItem> privateItems = new List<PsyItem>();

		static PsyItemStore ()
		{
		}

		public void sharedStore ()
		{
		}

		public PsyItem[] allItems()
		{
			return PsyItemStore.privateItems.ToArray ();
		}

		public PsyItem createItem()
		{
			PsyItem item = new PsyItem ().RandomItem ();
			PsyItemStore.privateItems.Add (item);

			return item;
		}
	}
}


Homepwner.Xamarin
=================

Simple iOS App Ported to CSharp from Objective-C

Following the book "iOS Programming - The Big Nerd Ranch Guide, 4th Edition", this 
is an iOS app that will eventually allow users to catalog all items in their house 
so that in the unfortunate case that something bad happens (fire, earthquake, meteor, 
wrath of God, etc.) they have a handy list of items to show their insurance company.

This version of the app is a port of the Homepwner.Xcode project, also here on github.
I am attempting to write both versions simultaneously so that I can both learn the
Cocoa Touch frameworks (and know how to program applications for iPhone) and also see
how/if they translate as seamlessly to Xamarin/Mono Touch as their evangelists would
have us believe.

Eventually, if all goes well, this version of the app will also be extended to include
Android and Windows Phone support through Xamarin Studio.

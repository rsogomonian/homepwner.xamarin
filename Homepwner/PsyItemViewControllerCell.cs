﻿using System;
using System.Drawing;
using MonoTouch.Foundation;
using MonoTouch.UIKit;

namespace Homepwner
{
	public class PsyItemViewControllerCell : UITableViewCell
	{
		public static readonly NSString Key = new NSString ("PsyItemViewControllerCell");

		public PsyItemViewControllerCell () : base (UITableViewCellStyle.Default, Key)
		{
			// TODO: add subviews to the ContentView, set various colors, etc.
			PsyItem item = new PsyItem().RandomItem ();
			TextLabel.Text = String.Format("{0}", item.ToString( ));
		}
	}
}

